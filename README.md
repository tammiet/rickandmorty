# Rick and Morty Cards

### How to run
To run the project, clone the repository locally and run `npm install` within the
repo, then run `npm start`. It should open a tab in the browser at
`localhost:3000`

### Decisions
I decided to design the UI from a "mobile first" perspective. I had the idea to
create a carousel of cards, with the name and picture of the character  on the
front and the character's description on the back. The cards can be 'flipped' by
clicking on them.

### Improvements

####  Optimize use of GraphQL
I'm new to GraphQL and React hooks, these are some of the things
I'd like to improve on.
- Handle async requests from GraphQL better, by preventing page reloading,
  until request is resolved.
- Explore 'prefetching' items as user scrolls.

#### UX / Design
Improvements

- Implement better carousel attributes, only allowing one card to be 'focused'
    at a time. I would also add styling to make 'focused' card slightly larger, and
    centered.
- Allow users to move focus to 'next' or 'previous' card by tracking horizontal 'swiping' motion,
  and update the new 'focused' card.
- Optimize for desktop view. Enable grid view of cards.
    - Add pagination for grid view.
- Improve layout of 'filter' buttons
- Add search functionality

#### Code
- Add tests
- Make components more reusable
