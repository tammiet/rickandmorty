import { useState } from 'react';

/**
 * CardComponent renders a name and picture, related to the item.
 */
export const CardComponent = function (props) {
    let character = props.character;
    const [activeFace, flipCard] = useState("show-front");
    function handleFlip() {
        if (activeFace === 'show-front') {
            flipCard('show-back');
        } else {
            flipCard('show-front');
        }
    }

    return (
        <div className={`card ${activeFace}`} key={character.id} onClick={handleFlip.bind(this)}>
            <div className="card-inner">
                <div className="card-front">
                    <div className='card-header'>
                        <h3>{character.name}</h3>
                    </div>
                    <div className='card-content'>
                        <img src={character.image} alt={character.name} />
                    </div>
                </div>
                <div className="card-back">
                    <div className='card-header'>
                        <h3>Description</h3>
                    </div>
                    <div className='card-content'>
                        <div className="description">
                            {
                                Object.keys(character).map((key, index) => {
                                    if (key !== 'name' && key !== 'image') {
                                        return (<p key={index}><strong>{key}:</strong> {character[key]}</p>)
                                    } else {
                                        return null;
                                    }
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

/**
 * ListComponent renders list of cards
 */
export const ListComponent = function (props) {
    let count = props.count;
    let data = props.data
    let cards = data.slice(0, count).map((character, index) => (
        <CardComponent key={index} index={index} character={character} />
    ))

    return (
        <div className="card-deck">
            {cards}
        </div>)
}
