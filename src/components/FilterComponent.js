/**
 * Filter button, which updates list, when selected
 */
export const FilterComponent = function (props) {
    let status = props.status;
    return (
        <div className={`list-filter ${props.selected}`} onClick={props.onClick}>
            <div className="list-value">{status}</div>
        </div>
    )
}
