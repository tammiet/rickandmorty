import './App.scss';
import { useState } from 'react';
import { ListComponent } from './components/ListComponent'
import { FilterComponent } from './components/FilterComponent'
import { gql, useQuery } from '@apollo/client';

const GET_CHARACTERS = gql`
    query characters($selectedStatus: String!) {
        characters(filter: { status: $selectedStatus }) {
            info {
                count
            }
            results {
                id,
                name,
                image,
                status,
                species,
                gender,
                episode {
                    name,
                    episode
                }
            }
        }
    }`

/**
 * App which renders a list of items from an API. It is updated when a filter
 * is applied, and a new result is returned from the request.
 */
function App() {
    let count = 20; // Configures number of items that the ListComponent renders
    let statusArr = ['alive', 'dead', 'unknown'] // List of available filters

    const [selectedStatus, setStatusSelected] = useState("alive");
    function handleSelectedStatus(status) {
        setStatusSelected(status);
    }

    const { loading, error, data } = useQuery(
        GET_CHARACTERS, { variables: { selectedStatus } });
    if (loading) return null;
    if (error) return `Error! ${error.message}`

    // Clean data returned from GET_CHARACTERS
    let characters = []
    characters = data['characters']['results'].map((character) => {
        let characterObj = {};
        for (const prop in character) {
            if (character.hasOwnProperty(prop) && prop !== '__typename') {
                if (prop === 'episode') {
                    characterObj['# of episodes'] = character[prop].length;
                } else {
                    characterObj[prop] = character[prop];
                }
            }
        }
        return characterObj;
    });

    return (
        <div className="App">
            <header className="App-header">
                <h1>Rick and Morty</h1>
            </header>
            <div className="list-filters">
                {
                    statusArr.map((status, index) => {
                        let selected = (status === selectedStatus) ? 'selected' : '';
                        return <FilterComponent key={index} status={status} selected={selected}
                            onClick={handleSelectedStatus.bind(this, status)} />
                    })
                }
            </div>
            <ListComponent count={count} data={characters} />
        </div>
    );
}

export default App;
